package com.iptiq.bedev

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BeDevApplication

fun main(args: Array<String>) {
    runApplication<BeDevApplication>(*args)
}
