package com.iptiq.bedev.distribution

import com.iptiq.bedev.domain.BankAccountRepository
import com.iptiq.bedev.domain.CreateBankAccountUseCase
import com.iptiq.bedev.domain.IbanValidator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.money.Monetary

@Configuration
class BankAccountConfiguration {

    @Bean
    fun createBankAccountUseCase(bankAccountRepository: BankAccountRepository, ibanValidator: IbanValidator) =
        CreateBankAccountUseCase(
            bankAccountRepository,
            setOf("CHF", "EUR").map(Monetary::getCurrency).toSet(),
            ibanValidator
        )
}