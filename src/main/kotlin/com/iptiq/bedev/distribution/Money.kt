package com.iptiq.bedev.distribution

import org.javamoney.moneta.Money
import org.javamoney.moneta.function.MonetaryQueries
import javax.money.CurrencyUnit
import javax.money.Monetary
import javax.money.MonetaryAmount

fun String.asCurrency(): CurrencyUnit = Monetary.getCurrency(this)

fun Long.asMoney(currency: String): MonetaryAmount = Money.ofMinor(currency.asCurrency(), this)

fun CurrencyUnit.asRepresentation(): String = currencyCode

fun MonetaryAmount.asRepresentation(): Pair<String, Long> =
    currency.asRepresentation() to query(MonetaryQueries.convertMinorPart())
