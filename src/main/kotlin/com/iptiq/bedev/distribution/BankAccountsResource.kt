package com.iptiq.bedev.distribution

import com.iptiq.bedev.domain.*
import jakarta.validation.Valid
import jakarta.validation.constraints.NotBlank
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.FieldError
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/v1/bank-accounts")
class BankAccountsResource(
    private val createBankAccountUseCase: CreateBankAccountUseCase,
    private val bankAccountRepository: BankAccountRepository
) {

    @PostMapping
    fun createBankAccount(@Validated @RequestBody body: CreateBankAccountRepresentation): ResponseEntity<BankAccountRepresentation> {
        val bankAccount = createBankAccountUseCase.create(body.toDomain())
        return ResponseEntity.status(HttpStatus.CREATED).body(BankAccountRepresentation.of(bankAccount))
    }


    @GetMapping("/{id}")
    fun getBankAccount(@PathVariable("id") id: String): BankAccountRepresentation =
        bankAccountRepository.get(id).let { BankAccountRepresentation.of(it) }


    @ExceptionHandler(IllegalArgumentException::class)
    fun handleClientError(throwable: IllegalArgumentException): ResponseEntity<String> {
        logger.warn("IllegalArgumentException handled!", throwable)
        return ResponseEntity.badRequest().body(throwable.message)
    }

    @ExceptionHandler(MethodArgumentNotValidException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleJavaxValidationError(throwable: MethodArgumentNotValidException): String {
        logger.warn("MethodArgumentNotValidException handled!", throwable)
        return throwable.allErrors
            .mapNotNull { it as? FieldError }.joinToString(separator = ", ") { "${it.field} - ${it.defaultMessage}" }
    }

    @ExceptionHandler(BankAccountRepository.BankAccountNotFound::class)
    fun handleClientError(throwable: BankAccountRepository.BankAccountNotFound): ResponseEntity<String> {
        logger.warn("BankAccountNotFound handled!", throwable)
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(throwable.message)
    }

    companion object {
        private val logger = LoggerFactory.getLogger(BankAccountsResource::class.java)
    }
}

class CreateBankAccountRepresentation(
    @get:NotBlank
    val iban: String,
    @get:Valid
    val initialBalance: Balance
) {
    fun toDomain(): CreateBankAccountDetails = CreateBankAccountDetails(
        iban = Iban(iban),
        initialBalance = initialBalance.toMoney()
    )
}


class Balance(
    @get:NotBlank(message = "Currency must not be blank")
    val currency: String,
    val amount: Long
) {

    fun toMoney() = amount.asMoney(currency)
}


class BankAccountRepresentation(
    val id: String,
    val balance: Balance,
    val iban: String
) {
    companion object {
        fun of(bankAccount: BankAccount): BankAccountRepresentation {
            val (currency, amount) = bankAccount.balance.asRepresentation()
            return BankAccountRepresentation(
                id = bankAccount.id,
                balance = Balance(currency, amount),
                iban = bankAccount.iban.value
            )
        }
    }
}