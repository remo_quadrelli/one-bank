package com.iptiq.bedev.distribution.rest

import com.iptiq.bedev.domain.Iban
import com.iptiq.bedev.domain.IbanValidator
import org.springframework.web.client.RestOperations
import org.springframework.web.client.getForObject

class IbanValidatorRest(
    private val restOperations: RestOperations,
    private val validatorUrl: String

) : IbanValidator {
    override fun isValid(iban: Iban): Boolean =
        restOperations.getForObject<Response>("$validatorUrl/validate/{iban}", mapOf("iban" to iban.value)).valid

    private class Response(
        val valid: Boolean
    )
}