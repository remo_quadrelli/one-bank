package com.iptiq.bedev.distribution.rest

import com.iptiq.bedev.domain.IbanValidator
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class IbanConfiguration {

    @Bean
    fun ibanValidator(
        restTemplateBuilder: RestTemplateBuilder,
        @Value("\${iban.validator.url}") validatorUrl: String
    ): IbanValidator = IbanValidatorRest(restTemplateBuilder.build(), validatorUrl)
}