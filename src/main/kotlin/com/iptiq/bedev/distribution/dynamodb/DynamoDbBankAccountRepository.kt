package com.iptiq.bedev.distribution.dynamodb

import com.iptiq.bedev.distribution.asMoney
import com.iptiq.bedev.distribution.asRepresentation
import com.iptiq.bedev.domain.BankAccount
import com.iptiq.bedev.domain.BankAccountRepository
import com.iptiq.bedev.domain.Iban
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import software.amazon.awssdk.services.dynamodb.model.*

class DynamoDbBankAccountRepository(
    private val dynamoDbClient: DynamoDbClient
) : BankAccountRepository {


    init {
        if (TABLE_NAME !in dynamoDbClient.listTables().tableNames()) {
            dynamoDbClient.createTable(
                CreateTableRequest.builder()
                    .tableName(TABLE_NAME)
                    .attributeDefinitions(
                        AttributeDefinition.builder()
                            .attributeName(ATTRIBUTE_ID)
                            .attributeType(ScalarAttributeType.S)
                            .build()
                    )
                    .keySchema(
                        KeySchemaElement.builder()
                            .attributeName(ATTRIBUTE_ID)
                            .keyType(KeyType.HASH)
                            .build()
                    )
                    .provisionedThroughput(
                        ProvisionedThroughput.builder()
                            .writeCapacityUnits(1L)
                            .readCapacityUnits(1L)
                            .build()
                    )
                    .build()
            )
        }
    }

    override fun save(bankAccount: BankAccount) {
        val request = PutItemRequest.builder()
            .tableName(TABLE_NAME)
            .item(asItem(bankAccount))
            .build()

        dynamoDbClient.putItem(request)
    }

    private fun asItem(bankAccount: BankAccount) = buildMap<String, AttributeValue> {
        put(ATTRIBUTE_ID, AttributeValue.fromS(bankAccount.id))
        bankAccount.balance.asRepresentation().let { (currency, amount) ->
            put(ATTRIBUTE_CURRENCY, AttributeValue.fromS(currency));
            put(ATTRIBUTE_AMOUNT, AttributeValue.fromN(amount.toString()))
        }
        put(ATTRIBUTE_IBAN, AttributeValue.fromS(bankAccount.iban.value))
    }

    override fun get(id: String): BankAccount {
        val response = dynamoDbClient.getItem {
            it.tableName(TABLE_NAME)
            it.key(mapOf(ATTRIBUTE_ID to AttributeValue.fromS(id)))
        }

        return if (response.hasItem()) asBankAccount(response.item())
        else throw BankAccountRepository.BankAccountNotFound(id)
    }

    private fun asBankAccount(item: Map<String, AttributeValue>) = BankAccount(
        id = item.getValue(ATTRIBUTE_ID).s(),
        balance = item.getValue(ATTRIBUTE_AMOUNT).n().toLong().asMoney(item.getValue(ATTRIBUTE_CURRENCY).s()),
        iban = Iban(item.getValue(ATTRIBUTE_IBAN).s())
    )

    companion object {
        private const val TABLE_NAME = "bank-accounts"
        private const val ATTRIBUTE_ID = "id"
        private const val ATTRIBUTE_CURRENCY = "currency"
        private const val ATTRIBUTE_AMOUNT = "amount"
        private const val ATTRIBUTE_IBAN = "iban"
    }
}