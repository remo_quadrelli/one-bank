package com.iptiq.bedev.distribution.dynamodb

import com.iptiq.bedev.distribution.asMoney
import com.iptiq.bedev.distribution.asRepresentation
import com.iptiq.bedev.domain.BankAccount
import com.iptiq.bedev.domain.BankAccountRepository
import com.iptiq.bedev.domain.Iban
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient
import software.amazon.awssdk.enhanced.dynamodb.Key
import software.amazon.awssdk.enhanced.dynamodb.TableSchema
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey

class EnhancedDynamoDbBankAccountRepository(
    dynamoDbClient: DynamoDbEnhancedClient
) : BankAccountRepository {

    private val table = dynamoDbClient.table(TABLE_NAME, SCHEMA)

    init {
        if (runCatching { table.describeTable() }.isFailure) {
            table.createTable()
        }
    }

    override fun save(bankAccount: BankAccount) {
        val item = asItem(bankAccount)
        table.putItem(item)
    }

    private fun asItem(bankAccount: BankAccount) = DynamodbBankAccount().apply {
        id = bankAccount.id
        bankAccount.balance.asRepresentation().let {
            currency = it.first
            amount = it.second
        }
        iban = bankAccount.iban.value
    }

    override fun get(id: String): BankAccount {
        val item = table.getItem(Key.builder().partitionValue(id).build())
        return item?.let { asBankAccount(it) } ?: throw BankAccountRepository.BankAccountNotFound(id)
    }

    private fun asBankAccount(item: DynamodbBankAccount) = BankAccount(
        id = item.id,
        balance = item.amount.asMoney(item.currency),
        iban = Iban(item.iban)
    )

    @DynamoDbBean
    class DynamodbBankAccount {
        @get:DynamoDbPartitionKey
        lateinit var id: String
        lateinit var currency: String
        var amount: Long = 0
        lateinit var iban: String
    }

    companion object {
        private const val TABLE_NAME = "enhanced-bank-accounts"
        private val SCHEMA = TableSchema.fromBean(DynamodbBankAccount::class.java)
    }
}