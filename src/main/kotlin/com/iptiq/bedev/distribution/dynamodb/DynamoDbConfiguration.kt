package com.iptiq.bedev.distribution.dynamodb

import com.iptiq.bedev.domain.BankAccountRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import java.net.URI

@Configuration
@ConditionalOnProperty("amazon.dynamodb.enabled")
class DynamoDbConfiguration {

    @Bean
    fun dynamoDb(
        @Value("\${amazon.dynamodb.endpoint}") dynamoDbEndpoint: String,
        @Value("\${amazon.dynamodb.region}") region: String,
    ): DynamoDbClient = DynamoDbClient.builder()
        .credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create("ignore", "me")))
        .endpointOverride(URI.create(dynamoDbEndpoint))
        .region(Region.of(region))
        .build()

    @Bean
    @ConditionalOnProperty("amazon.dynamodb.enhanced", havingValue = "false", matchIfMissing = true)
    fun bankAccountRepository(dynamoDbClient: DynamoDbClient): BankAccountRepository =
        DynamoDbBankAccountRepository(dynamoDbClient)
            .also { logger.info("Using dynamodb repository") }

    @Bean
    @ConditionalOnProperty("amazon.dynamodb.enhanced")
    fun dynamoDbEnhancedClient(dynamoDbClient: DynamoDbClient): DynamoDbEnhancedClient =
        DynamoDbEnhancedClient.builder().dynamoDbClient(dynamoDbClient).build()

    @Bean
    @ConditionalOnProperty("amazon.dynamodb.enhanced")
    fun enhancedBankAccountRepository(enhancedClient: DynamoDbEnhancedClient) =
        EnhancedDynamoDbBankAccountRepository(enhancedClient)
            .also { logger.info("Using enhanced dynamodb repository") }


    companion object {
        private val logger = LoggerFactory.getLogger(DynamoDbConfiguration::class.java)
    }
}