package com.iptiq.bedev.distribution.inmemory

import com.iptiq.bedev.domain.BankAccountRepository
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class InMemoryConfiguration {

    @Bean
    @ConditionalOnMissingBean(BankAccountRepository::class)
    fun bankAccountRepository(): BankAccountRepository = InMemoryBankAccountRepository()
        .also { logger.info("Using in memory repository") }

    companion object {
        private val logger = LoggerFactory.getLogger(InMemoryConfiguration::class.java)
    }
}