package com.iptiq.bedev.distribution.inmemory

import com.iptiq.bedev.domain.BankAccount
import com.iptiq.bedev.domain.BankAccountRepository

class InMemoryBankAccountRepository : BankAccountRepository {

    private val bankAccounts = mutableMapOf<String, BankAccount>()

    override fun save(bankAccount: BankAccount) {
        bankAccounts[bankAccount.id] = bankAccount
    }

    override fun get(id: String): BankAccount = bankAccounts[id] ?: throw BankAccountRepository.BankAccountNotFound(id)
}