package com.iptiq.bedev.domain

interface BankAccountRepository {

    fun save(bankAccount: BankAccount)

    fun get(id: String): BankAccount

    class BankAccountNotFound(id: String) : RuntimeException("BankAccount $id not found!")
}