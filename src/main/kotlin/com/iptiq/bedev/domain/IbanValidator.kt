package com.iptiq.bedev.domain

fun interface IbanValidator {

    fun isValid(iban: Iban): Boolean
}