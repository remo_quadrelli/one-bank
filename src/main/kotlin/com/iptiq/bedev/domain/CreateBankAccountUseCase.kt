package com.iptiq.bedev.domain

import org.slf4j.LoggerFactory
import java.util.*
import javax.money.CurrencyUnit
import javax.money.MonetaryAmount

class CreateBankAccountUseCase(
    private val bankAccountRepository: BankAccountRepository,
    private val supportedCurrencies: Set<CurrencyUnit>,
    private val ibanValidator: IbanValidator
) {

    fun create(details: CreateBankAccountDetails): BankAccount {
        require(details.initialBalance.currency in supportedCurrencies) { "Currency ${details.initialBalance.currency} is not supported" }
        require(ibanValidator.isValid(details.iban)) { "Iban ${details.iban.value} is not valid" }
        return bankAccountFor(details)
            .also(bankAccountRepository::save)
            .also { logger.info("New bank account ${it.id} created with initial balance ${it.balance}") }
    }

    private fun bankAccountFor(details: CreateBankAccountDetails) = BankAccount(
        id = UUID.randomUUID().toString(),
        balance = details.initialBalance,
        iban = details.iban
    )

    companion object {
        private val logger = LoggerFactory.getLogger(CreateBankAccountUseCase::class.java)
    }
}

data class CreateBankAccountDetails(
    val iban: Iban,
    val initialBalance: MonetaryAmount
) {

    init {
        require(initialBalance.isPositiveOrZero) { "Negative balance not allowed" }
    }
}