package com.iptiq.bedev.domain

import javax.money.MonetaryAmount

data class BankAccount(
    val id: String,
    val balance: MonetaryAmount,
    val iban: Iban
)