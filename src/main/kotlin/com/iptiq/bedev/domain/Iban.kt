package com.iptiq.bedev.domain

data class Iban(val value: String)