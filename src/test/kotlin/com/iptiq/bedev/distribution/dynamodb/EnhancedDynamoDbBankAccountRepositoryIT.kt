package com.iptiq.bedev.distribution.dynamodb

import com.iptiq.bedev.domain.BankAccount
import com.iptiq.bedev.domain.BankAccountRepository
import com.iptiq.bedev.domain.Iban
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import org.javamoney.moneta.Money
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.TestConstructor
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient
import java.util.*

@SpringBootTest
@AutoConfigureMockMvc
@TestConstructor(autowireMode = TestConstructor.AutowireMode.ALL)
class EnhancedDynamoDbBankAccountRepositoryIT(
    dynamoDbClient: DynamoDbEnhancedClient
) {

    private val victim = EnhancedDynamoDbBankAccountRepository(dynamoDbClient)

    @Test
    fun `should store and read bank accounts`() {
        val aBankAccount = anAccount()

        victim.save(aBankAccount)

        victim.get(aBankAccount.id) shouldBe aBankAccount
    }

    @Test
    fun `should throw on non existing bank account`() {
        shouldThrow<BankAccountRepository.BankAccountNotFound> { victim.get(anAccount().id) }
    }

    private fun anAccount() = BankAccount(
        id = UUID.randomUUID().toString(),
        balance = Money.parse("EUR 150.50"),
        iban = Iban("a-random-iban")
    )
}