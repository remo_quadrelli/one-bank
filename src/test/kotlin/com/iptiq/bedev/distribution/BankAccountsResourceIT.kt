package com.iptiq.bedev.distribution

import com.iptiq.bedev.domain.BankAccount
import com.iptiq.bedev.domain.BankAccountRepository
import com.iptiq.bedev.domain.Iban
import org.javamoney.moneta.Money
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.context.TestConstructor
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import java.util.*

@SpringBootTest
@AutoConfigureMockMvc
@TestConstructor(autowireMode = TestConstructor.AutowireMode.ALL)
class BankAccountsResourceIT(
    private val mockMvc: MockMvc,
    private val bankAccountRepository: BankAccountRepository
) {

    @Test
    fun `should create a new bank account`() {
        mockMvc.post("/api/v1/bank-accounts") {
            contentType = MediaType.APPLICATION_JSON
            content = """
                {
                    "iban": "CH2789144783945699531",
                    "initialBalance": {
                        "currency": "EUR",
                        "amount": 10050
                    }
                }
            """.trimIndent()
        }
            .andExpect {
                status { isCreated() }
                header { stringValues(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE) }
                jsonPath("id") { exists() }
                jsonPath("iban") { value("CH2789144783945699531") }
                jsonPath("balance.currency") { value("EUR") }
                jsonPath("balance.amount") { value(100_50) }
            }
    }

    @Test
    fun `should fail on invalid iban`() {
        mockMvc.post("/api/v1/bank-accounts") {
            contentType = MediaType.APPLICATION_JSON
            content = """
                {
                    "iban": "invalid-iban",
                    "initialBalance": {
                        "currency": "EUR",
                        "amount": 10050
                    }
                }
            """.trimIndent()
        }
            .andExpect {
                status { isBadRequest() }
                content { string("Iban invalid-iban is not valid") }
            }
    }

    @Test
    fun `should fail on negative initial balance - rejected by CreateBankAccountDetails`() {
        mockMvc.post("/api/v1/bank-accounts") {
            contentType = MediaType.APPLICATION_JSON
            content = """
                {
                    "iban": "CH2789144783945699531",
                    "initialBalance": {
                        "currency": "EUR",
                        "amount": -10050
                    }
                }
            """.trimIndent()
        }
            .andExpect {
                status { isBadRequest() }
                content { string("Negative balance not allowed") }
            }
    }


    @Test
    fun `should fail on unsupported currency - rejected by CreateBankAccountUseCase`() {
        mockMvc.post("/api/v1/bank-accounts") {
            contentType = MediaType.APPLICATION_JSON
            content = """
                {
                    "iban": "CH2789144783945699531",
                    "initialBalance": {
                        "currency": "JPY",
                        "amount": 10050
                    }
                }
            """.trimIndent()
        }
            .andExpect {
                status { isBadRequest() }
                content { string("Currency JPY is not supported") }
            }
    }

    @Test
    fun `should fail on blank currency - rejected by javax validation on Balance#currency`() {
        mockMvc.post("/api/v1/bank-accounts") {
            contentType = MediaType.APPLICATION_JSON
            content = """
                {
                    "iban": "CH2789144783945699531",
                    "initialBalance": {
                        "currency": "  ",
                        "amount": 10050
                    }
                }
            """.trimIndent()
        }
            .andExpect {
                status { isBadRequest() }
                content { string("initialBalance.currency - Currency must not be blank") }
            }
    }


    @Test
    fun `should fail on unknown bank account - rejected by custom exception`() {
        val unknownId = UUID.randomUUID().toString()
        mockMvc.get("/api/v1/bank-accounts/$unknownId")
            .andExpect {
                status { isNotFound() }
                content { string("BankAccount $unknownId not found!") }
            }
    }

    @Test
    fun `should return already existing bank account`() {
        val bankAccountId = aRandomBankAccount()

        mockMvc.get("/api/v1/bank-accounts/$bankAccountId")
            .andExpect {
                status { isOk() }
                jsonPath("id") { value(bankAccountId) }
                jsonPath("iban") { value("CH2789144783945699531") }
                jsonPath("balance.currency") { value("EUR") }
                jsonPath("balance.amount") { value(0) }
            }
    }

    private fun aRandomBankAccount() = BankAccount(
        id = UUID.randomUUID().toString(),
        balance = Money.zero("EUR".asCurrency()),
        iban = Iban("CH2789144783945699531")
    )
        .also { bankAccountRepository.save(it) }
        .id

}