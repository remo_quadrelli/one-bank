package com.iptiq.bedev.distribution.rest

import com.iptiq.bedev.domain.Iban
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.test.context.TestConstructor
import org.springframework.test.web.client.MockRestServiceServer
import org.springframework.test.web.client.match.MockRestRequestMatchers.method
import org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo
import org.springframework.test.web.client.response.MockRestResponseCreators
import org.springframework.test.web.client.response.MockRestResponseCreators.withServerError
import org.springframework.web.client.HttpServerErrorException

@SpringBootTest
@TestConstructor(autowireMode = TestConstructor.AutowireMode.ALL)
class IbanValidatorRestIT(
    restTemplateBuilder: RestTemplateBuilder
) {

    private val restTemplate = restTemplateBuilder.build()

    private val remoteService = MockRestServiceServer.createServer(restTemplate)

    private val victim = IbanValidatorRest(restTemplate, "remoteUrl")

    private val iban = "CH2789144783945699531"

    @BeforeEach
    fun setup() {
        remoteService.reset()
    }

    @Test
    fun `should validate valid iban`() {
        remoteService.expect(requestTo("/remoteUrl/validate/$iban"))
            .andExpect(method(HttpMethod.GET))
            .andRespond(
                MockRestResponseCreators.withSuccess(
                    """
                      {
                         "valid": true,
                         "messages": [],
                         "iban": "$iban"
                      }   
                  """.trimIndent(),
                    MediaType.APPLICATION_JSON
                )
            )

        victim.isValid(Iban(iban)) shouldBe true
    }

    @Test
    fun `should validate invalid iban`() {
        remoteService.expect(requestTo("/remoteUrl/validate/invalid-iban"))
            .andExpect(method(HttpMethod.GET))
            .andRespond(
                MockRestResponseCreators.withSuccess(
                    """
                      {
                         "valid": false,
                         "messages": [
                            "Cannot parse as IBAN: Invalid / no check digits found."
                         ],
                         "iban": "invalid-iban"
                      }   
                  """.trimIndent(),
                    MediaType.APPLICATION_JSON
                )
            )

        victim.isValid(Iban("invalid-iban")) shouldBe false
    }

    @Test
    fun `should throw on error response`() {
        remoteService.expect(requestTo("/remoteUrl/validate/$iban"))
            .andExpect(method(HttpMethod.GET))
            .andRespond(withServerError())

        shouldThrow<HttpServerErrorException> { victim.isValid(Iban(iban)) }
    }
}