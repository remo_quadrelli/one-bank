package com.iptiq.bedev.domain

import com.iptiq.bedev.distribution.asCurrency
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import org.javamoney.moneta.Money
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import java.math.BigDecimal
import javax.money.Monetary

class CreateBankAccountUseCaseTest {

    private val bankAccountRepository = mock<BankAccountRepository>()
    private val supportedCurrencies = setOf("EUR").map(Monetary::getCurrency).toSet()
    private val ibanValidator = mock<IbanValidator>()

    private val victim = CreateBankAccountUseCase(bankAccountRepository, supportedCurrencies, ibanValidator)

    @Test
    fun `should create a new bank account`() {
        val details = CreateBankAccountDetails(
            initialBalance = Money.of(BigDecimal.TEN, "EUR".asCurrency()),
            iban = Iban("a-random-iban")
        )
        whenever(ibanValidator.isValid(details.iban)).doReturn(true)

        val bankAccount = victim.create(details)

        bankAccount.balance shouldBe details.initialBalance
        bankAccount.iban shouldBe details.iban

        verify(bankAccountRepository).save(bankAccount)
    }

    @Test
    fun `should throw if unsupported currency is used`() {
        val details = CreateBankAccountDetails(
            initialBalance = Money.of(BigDecimal.TEN, "CHF".asCurrency()),
            iban = Iban("a-random-iban")
        )

        shouldThrow<IllegalArgumentException> { victim.create(details) }

        verify(bankAccountRepository, never()).save(any())
    }

    @Test
    fun `should throw if invalid iban is used`() {
        val details = CreateBankAccountDetails(
            initialBalance = Money.of(BigDecimal.TEN, "EUR".asCurrency()),
            iban = Iban("an-invalid-iban")
        )
        whenever(ibanValidator.isValid(details.iban)).doReturn(false)

        shouldThrow<IllegalArgumentException> { victim.create(details) }

        verify(bankAccountRepository, never()).save(any())
    }
}